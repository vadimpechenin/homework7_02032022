package classesForJar.person1;

import plugin.Plugin;

public class Person implements Plugin {
    //Первый класс, производит умножение возраста на заданное число
    public String name;
    public int age;

    public Person() {
        this.name = "Коля";
        this.age = 20;
    }

    public int ageToNumber(int number){
        return age*number;
    }

   /* public static void main(String[] args) {

    }*/

    @Override
    public String toString() {
        return "Person1{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void doUsefull() {
        int result = ageToNumber(10);
        System.out.println("Объект класса Person 1 в результате получил " + Integer.toString(result));
    }
}
