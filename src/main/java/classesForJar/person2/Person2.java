package classesForJar.person2;

import plugin.Plugin;

public class Person2 implements Plugin {
    //Второй класс, производит деление возраста на заданное число
    public String name;
    public int age;

    public Person2() {
        this.name = "Вася";
        this.age = 30;
    }

    public int ageToNumber(int number){
        return age/number;
    }

    /*public static void main(String[] args) {

    }*/

    @Override
    public String toString() {
        return "Person2{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void doUsefull() {
        int result = ageToNumber(10);
        System.out.println("Объект класса Person 2 в результате получил " + Integer.toString(result));
    }
}
