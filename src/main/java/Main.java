/*
Программа загружает два упакованных класса-плагина, в каждом упакован класс Person
 */

import plugin.Plugin;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException, IllegalAccessException, InstantiationException {
        takeListOfPlugins();
               /* Person person = new Person();
        System.out.println(2);
        Class<? extends Person> aClass1 = person.getClass();
*/
       /* URL[] urls = new URL[]{URI.create("file:D:/JAVA/SberSchool/Programms/HomeWork7_02032022/target/classes/Person.class").toURL()};
        URLClassLoader urlClassLoader = new URLClassLoader(urls, null);*/


/*        URL url = new URL("file:D:/JAVA/SberSchool/Programms/HomeWork7_02032022/person1.jar");
        URLClassLoader myJarClassLoader  =   new  URLClassLoader( new  URL[]   { url } );
        Class<?> myClass  =  myJarClassLoader.loadClass("Person");
        Plugin action  =  (Plugin)myClass.newInstance();*/
    }

    public static boolean takeListOfPlugins() throws IllegalAccessException, InstantiationException, ClassNotFoundException, MalformedURLException {
        String path = ("file:D:/JAVA/SberSchool/Programms/HomeWork7_02032022/");
        PluginManager manager = new PluginManager(path);
        //Загрузка двух плагинов - классов Person
        String pluginName = "person1.jar";
        String pluginClassName = "classesForJar.person1.Person";
        ArrayList<Plugin> plugins= new ArrayList<Plugin>();
        plugins.add(manager.load(pluginName, pluginClassName));
        pluginName = "person2.jar";
        pluginClassName = "classesForJar.person2.Person2";
        plugins.add(manager.load(pluginName, pluginClassName));

        URL url1 = new URL("file:D:/JAVA/SberSchool/Programms/HomeWork7_02032022/target/classes/Person.class");
        URLClassLoader urlClassLoader  =   new  URLClassLoader( new  URL[]   { url1 } );
        Class<?> aClass2 = urlClassLoader.loadClass("Person");
        plugins.add((Plugin)aClass2.newInstance());
        for (Plugin item:plugins){
            item.doUsefull();
        }
        return true;
    }
}