import plugin.Plugin;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

public class PluginManager {
    private final String pluginRootDirectory ;
    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }
    public Plugin load(String pluginName, String pluginClassName) throws MalformedURLException, ClassNotFoundException, IllegalAccessException, InstantiationException {

        URL url = new URL(pluginRootDirectory +pluginName);
        URLClassLoader myJarClassLoader  =   new  URLClassLoader( new  URL[]   { url } );
        Class<?> myClass  =  myJarClassLoader.loadClass(pluginClassName);
        return (Plugin)myClass.newInstance();
    }
}
