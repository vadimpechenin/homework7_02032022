import plugin.Plugin;

public class Person implements Plugin {
    //Класс вне jar файла, производит сложение возраста и числа
    public String name;
    public int age;

    public Person() {
        this.name = "Дима";
        this.age = 28;
    }

    public int ageToNumber(int number){
        return age+number;
    }

    @Override
    public String toString() {
        return "PersonWithoutJar{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void doUsefull() {
        int result = ageToNumber(10);
        System.out.println("Объект класса Person не из jar в результате получил " + Integer.toString(result));
    }
}
